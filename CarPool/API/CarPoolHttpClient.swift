//
//  CarPoolHttpClient.swift
//  CarPool
//
//  Created by Apple on 9/15/18.
//  Copyright © 2018 iSwiftful. All rights reserved.
//

import UIKit
import RealmSwift

typealias JSONDictionary = Dictionary<String, AnyObject>
typealias JSONArray = Array<AnyObject>

class CarPoolHttpClient: NSObject, URLSessionDelegate, URLSessionTaskDelegate, URLSessionDataDelegate  {
    
    var responseData:NSMutableData = NSMutableData()
    var requestStartTime:NSDate = NSDate()
    let opQueue = OperationQueue()
     var session:URLSession?
    enum Path {
        case get_CAR_LOCATIONS
    }
    typealias HttpAPICallback = ((AnyObject?, Error?) -> ())
    //let responseData = NSMutableData()
    var statusCode:Int = -1
    var callback: HttpAPICallback! = nil
    var path: Path! = nil
    
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        // When session task is complete, this delegate method will be called.
        // If there is no error then NSError object will nil, otherwise NSError
        // will contain information about the error.
        if let errorInfo = error{
            print("Session error: \(errorInfo.localizedDescription)")
        }
        else{
            print("Request - complete!")
            print("Total data downloaded - \(responseData.length) bytes")
            print("Total request time: \(-requestStartTime.timeIntervalSinceNow) seconds")
            // Now you can process the data.
            var json : Any!
            switch(statusCode, self.path!) {
            case (200, Path.get_CAR_LOCATIONS):
                do {
                    json = try JSONSerialization.jsonObject(with: self.responseData as Data, options: JSONSerialization.ReadingOptions.mutableLeaves)
                } catch {
                    print(error.localizedDescription)
                    json = nil
                }
                if (error != nil) {
                    callback(nil, error)
                    return
                }
                callback?(self.handleCarLocations(json: json) as AnyObject?, nil)
            default:
                // Unknown Error
                callback(nil, nil)
            }
        }
    }

    func urlSession(_ session: URLSession, task: URLSessionTask, willPerformHTTPRedirection response: HTTPURLResponse, newRequest request: URLRequest, completionHandler: @escaping (URLRequest?) -> Void) {
            print("Redirecting request!")
            // For bypass redirect, create new URL request and assign it incoming
            // new request object. And then call completion handler.
        let newRequest:NSURLRequest? = request as NSURLRequest
        print(newRequest?.description as Any)
        completionHandler((newRequest! as URLRequest))

    }
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
        print("Session received first response!")
        
        responseData = NSMutableData()
        
        let httpResponse = response as! HTTPURLResponse
        statusCode = httpResponse.statusCode
        switch (httpResponse.statusCode) {
        case 201, 200, 401:
            self.responseData.length = 0
        default:
            print("ignore")
        }
        // It is necessary to call completionHandler, otherwise request
        // will not progress one way or the other
        completionHandler(.allow)

    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        responseData.append(data)
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, willCacheResponse proposedResponse: CachedURLResponse, completionHandler: @escaping (CachedURLResponse?) -> Void) {
        print("Session response caching!")
        completionHandler(proposedResponse)
    }
    
    //func sessionDidFinishLoading(_ response: HTTPURLResponse) {
//        var error: NSError?
//        var json : Any!
//        var stringVal : String!
//        switch(statusCode, self.path!) {
//        case (200, Path.get_CAR_LOCATIONS):
//            do {
//                json = try JSONSerialization.jsonObject(with: self.responseData as Data, options: JSONSerialization.ReadingOptions.mutableLeaves)
//            } catch let error1 as NSError {
//                error = error1
//                json = nil
//            }
//            if (error != nil) {
//                callback(nil, error)
//                return
//            }
//            callback?(self.handleCarLocations(json: json) as AnyObject?, nil)
//        default:
//            // Unknown Error
//            callback(nil, nil)
//        }
   // }
    
    func handleCarLocations(json: Any) -> [NSDictionary]? {
        if let items = json as? NSDictionary
        {
            if items["placemarks"] != nil{
                return items["placemarks"] as? [NSDictionary]
            }
            return nil
        }
        else
        {
            return nil
        }
    }
   
    
    func handleInternalError(_ json: AnyObject) -> NSError {
        if let _ = json as? String {
            return NSError(domain:CPConstants.AlertViewTypes.Info.serverError, code:500, userInfo:["\(CPConstants.AlertViewTypes.Info.serverError)": "500 - Internal Error."])
        }
        return NSError(domain:CPConstants.AlertViewTypes.Info.serverError, code:500, userInfo:["\(CPConstants.AlertViewTypes.Info.serverError)": "500 - Internal Error."])
    }
    
    func handleNotInternetConnectionError() -> NSError {
        return NSError(domain:CPConstants.AlertViewTypes.Info.serverError, code:1009, userInfo:["\(CPConstants.AlertViewTypes.Info.serverError)": "\(CPConstants.AlertViewTypes.Info.NotConnectToServer)"])
    }
    
    func handleNotAllowedError(_ json: AnyObject) -> NSError {
        if let _ = json as? String {
            return NSError(domain:CPConstants.AlertViewTypes.Info.serverError, code:405, userInfo:["\(CPConstants.AlertViewTypes.Info.serverError)": "405 - Method Not Allowed."])
        }
        return NSError(domain:CPConstants.AlertViewTypes.Info.serverError, code:405, userInfo:["\(CPConstants.AlertViewTypes.Info.serverError)": "405 - Method Not Allowed."])
    }
    
    func handleNotFoundError(_ json: AnyObject) -> NSError {
        
        if let _ = json as? String {
            return NSError(domain:CPConstants.AlertViewTypes.Info.serverError, code:404, userInfo:["\(CPConstants.AlertViewTypes.Info.serverError)": "404 - File or directory not found."])
        }
        return NSError(domain:CPConstants.AlertViewTypes.Info.serverError, code:404, userInfo:["\(CPConstants.AlertViewTypes.Info.serverError)": "404 - File or directory not found."])
    }
    
    
    
    
    
    func getAllCarLocations_webApi(_ callback: @escaping HttpAPICallback) {
        if CPConstants.baseURL != ""{
//            let _urlPath = "\(CPConstants.baseURL)/locations.json​"
//            guard let url = NSURL(string: _urlPath.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
//                else
//            {
//                print("Couldn't parse myURL = \(_urlPath)")
//                return
//            }
            
            let url = NSURL(string:"https://s3-us-west-2.amazonaws.com/wunderbucket/locations.json")
            print("Request url is: \(String(describing: url))!")
            self.path = Path.get_CAR_LOCATIONS
            self.callback = callback
            print("Fetching Car Locations - Start")
            print("Request url is: \(String(describing: url))!")
            // Step0: Create request object.
            let request = NSMutableURLRequest(url:url! as URL)
            // Step1: Create configuration
            let sessionConfiguration = URLSessionConfiguration.default

            // Step2: Create session object with appropriate configuration, delegate and delegate queue
            // initialize the URLSession
            self.session = URLSession(configuration: sessionConfiguration, delegate: self, delegateQueue: OperationQueue.main)

            // Step3: Create session task
            let sessionTask = self.session?.dataTask(with: request as URLRequest)
            requestStartTime = NSDate()
            sessionTask?.resume()
        }
        else
        {
            CPViewController.displayAlertInfoBox(CPConstants.AlertViewTypes.Info.ErrorTitle, message: CPConstants.AlertViewTypes.Info.emptyURL)
        }
    }
}
