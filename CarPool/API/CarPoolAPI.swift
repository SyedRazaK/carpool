//
//  CabAPI.swift
//  iCAB
//
//  Created by Apple on 9/12/18.
//  Copyright © 2018 iSwiftful. All rights reserved.
//

import Foundation
import RealmSwift

class CarPoolAPI: NSObject, PersistencyManagerProtocol, NSURLConnectionDataDelegate {
    fileprivate let persistencyManager: PersistencyManager
    fileprivate let httpClient: CarPoolHttpClient
    typealias APICallback = ((AnyObject?, Error?) -> ())
    
    var callback: APICallback! = nil
    
    static let sharedInstance = CarPoolAPI()
    override init() {
        persistencyManager = PersistencyManager()
        httpClient = CarPoolHttpClient()
        super.init()
    }
    
    func loadCarLocations(_ callback:@escaping APICallback){
        httpClient.getAllCarLocations_webApi({ (data, error) in
            if (error == nil) {
                if data != nil{
                    let _ = PersistencyManager(items: data, tableModel: PlaceMark())
                    callback(data,error)
                }
            }
        })
    }
    
    func getPlaceMarks() -> NSMutableArray! {
        return persistencyManager.getPlaceMarks()
    }
    
}
