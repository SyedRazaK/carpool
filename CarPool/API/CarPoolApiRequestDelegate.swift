//
//  CarPoolApiRequestDelegate.swift
//  CarPool
//
//  Created by Apple on 9/15/18.
//  Copyright © 2018 iSwiftful. All rights reserved.
//

import Foundation
/*:
 # Simple delegate for NSURLSession related request/response mechanism
 Create new instance for each session task.
 */
public class CarPoolApiRequestDelegate.swift:NSObject, NSURLSessionDelegate, NSURLSessionTaskDelegate,
NSURLSessionDataDelegate{
    public var requestTime:NSTimeInterval?
    public var response:HTTPURLResponse?
    var completionHandler:((HTTPURLResponse) -> Void)?
    
    /*:
     This delegate method is called once when response is recieved. This is the place where
     you can perform initialization or other related tasks before start recieviing data
     from response
     */
    public func URLSession(session: NSURLSession, dataTask: NSURLSessionDataTask,
                           didReceiveResponse response: NSURLResponse,
                           completionHandler: (NSURLSessionResponseDisposition) -> Void) {
        print("Session received first response!")
        self.response = HttpResponse(response: response as! NSHTTPURLResponse)
        // It is necessary to call completionHandler, otherwise request
        // will not progress one way or the other
        completionHandler(NSURLSessionResponseDisposition.Allow)
    }
    /*:
     This delegate method is called when session task is finished. Check for presence
     of NSError object to decide if call was successful or not
     */
    public func URLSession(session: NSURLSession, task: NSURLSessionTask,
                           didCompleteWithError error: NSError?) {
        // When session task is complete, this delegate method will be called.
        // If there is no error then NSError object will nil, otherwise NSError
        // will contain information about the error.
        if let errorInfo = error{
            print("Session error: \(errorInfo.description)")
            self.response?.error = error
        }
        else{
            print("Request - complete!")
            self.response?.responseUrl = task.response?.URL
            self.response?.statusCode = (task.response as! NSHTTPURLResponse).statusCode
        }
        if let compHandler = completionHandler{
            compHandler(self.response!)
        }
    }
    /*:
     This delegate method is called when response data is recieved in chunks or
     in one shot.
     */
    public func URLSession(session: NSURLSession, dataTask: NSURLSessionDataTask,
                           didReceiveData data: NSData) {
        response?.responseData.appendData(data)
    }
}
