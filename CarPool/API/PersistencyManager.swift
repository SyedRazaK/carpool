//
//  PersistencyManager.swift
//  iCAB
//
//  Created by Apple on 9/12/18.
//  Copyright © 2018 iSwiftful. All rights reserved.
//

import RealmSwift

protocol PersistencyManagerProtocol {
    func getPlaceMarks() -> NSMutableArray!
}

class PersistencyManager : NSObject,PersistencyManagerProtocol {
    
    var placeMarks    = try! Realm().objects(PlaceMark.self)
    
    override init() {
        super.init()
    }
    
     convenience init(items:AnyObject?,tableModel: AnyObject?){
        self.init()
        
        if let pModel = tableModel as? PlaceMark{
             if placeMarks.count == 0 {
                fillModelWithData(items, tableModel: pModel)
             }
        }
    }
    
    func fillModelWithData(_ items:AnyObject? , tableModel: AnyObject?){
        do {
            let realm = try Realm()
            do {
                realm.beginWrite()
                if let _ = tableModel as? PlaceMark{
                    for item in items as! [NSDictionary]{
                        let pMark = PlaceMark()
                        pMark.name = item["name"] as! String
                        pMark.address = item["address"] as! String
                        pMark.engineType = item["engineType"] as! String
                        pMark.interior = item["interior"] as! String
                        pMark.exterior = item["exterior"] as! String
                        pMark.vin = item["vin"] as! String
                        pMark.fuel = item["fuel"] as! Int
                        
                        let _cords = item["coordinates"] as! NSArray
                        let coOrdinates =  Coordinates()
                        coOrdinates.lon = _cords[0] as! Double
                        coOrdinates.lat = _cords[1] as! Double
                        pMark.cords = coOrdinates
                        realm.create(PlaceMark.self, value: pMark, update: true)
                    }
                }
                try! realm.commitWrite()
//                placeMarks    = try! Realm().objects(PlaceMark.self)
            }
        }
        catch let error as NSError {
            print(error)
        }
    }
    
    func getPlaceMarks() ->  NSMutableArray! {
        let pMarks = NSMutableArray()
        if placeMarks.count > 0 {
            for placeMark in placeMarks{
                pMarks.add(placeMark)
            }
        }
        return pMarks
    }
    
}
