//
//  CarMarkerView.swift
//  CarPool
//
//  Created by Apple on 9/16/18.
//  Copyright © 2018 iSwiftful. All rights reserved.
//

import MapKit

class CarMarkerView: MKAnnotationView {
    override var annotation: MKAnnotation? {
        willSet {
            guard let carMarker = newValue as? CarMarker else {return}
            canShowCallout = true
            calloutOffset = CGPoint(x: -5, y: 5)
            let mapsButton = UIButton(frame: CGRect(origin: CGPoint.zero,
                                                    size: CGSize(width: 30, height: 30)))
            mapsButton.setBackgroundImage(UIImage(named: "ic_AppleMap"), for: UIControlState())
            rightCalloutAccessoryView = mapsButton
            
            if let imageName = carMarker.imageName {
                image = UIImage(named: imageName)
            } else {
                image = nil
            }
        }
    }
}
