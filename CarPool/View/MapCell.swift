//
//  MapCell.swift
//  CarPool
//
//  Created by Apple on 9/16/18.
//  Copyright © 2018 iSwiftful. All rights reserved.
//

import UIKit
import MapKit
import Contacts

class MapCell : BaseCell{
    
    let regionRadius: CLLocationDistance = 500
    let cellId = "mapCellId"
    
    lazy var btnUserLocation: UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "ic_zoom_myLocation"), for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(zoomToUserCurrentLocation), for: .touchUpInside)
        return btn
    }()
    
    lazy var mapView : MKMapView = {
       let mapView = MKMapView()
        mapView.mapType = MKMapType.standard
        mapView.isZoomEnabled = true
        mapView.showsUserLocation = true
        mapView.isScrollEnabled = true
        mapView.userTrackingMode = .follow
        mapView.delegate = self
        return mapView
    }()
    
    override func setupViews() {
        addSubview(mapView)
        addSubview(btnUserLocation)
        
        addConstraintsWithFormat(format: "H:|[v0]|", views: mapView)
        addConstraintsWithFormat(format: "V:|[v0]|", views: mapView)
        
        addConstraintsWithFormat(format: "H:[v0]-20-|", views: btnUserLocation)
        addConstraintsWithFormat(format: "V:[v0]-20-|", views: btnUserLocation)
        
        
        setupMapView()
        checkLocationAuthorizationStatus()
    }
    
    let locationManager = CLLocationManager()
    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            mapView.showsUserLocation = true
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func setupMapView(){
        let initialLocation = CLLocation(latitude: 53.54484 , longitude: 9.99981)
        centerMapOnLocation(location: initialLocation)
        mapView.register(CarMarkerView.self,
                         forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        setupAnnotations()
    }
    
    @objc func zoomToUserCurrentLocation() {
        if self.mapView != nil {
            self.mapView.setRegion(MKCoordinateRegionMake(
                self.mapView.userLocation.coordinate,
                MKCoordinateSpanMake(0.1, 0.1)
            ), animated: true)
        }
    }
    
    func setupAnnotations(){
        var annotations = [MKAnnotation]();
        for placeMarker in CPStore.sharedInstance.placeMarks{

            let mv = CarMarker(title: placeMarker.name, locationName: placeMarker.address, discipline: "CarPool", coordinate: CLLocationCoordinate2D(latitude: (placeMarker.cords?.lat)!, longitude: (placeMarker.cords?.lon)!))

            annotations.append(mv)
        }

        mapView.addAnnotations(annotations)
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius, regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
}

extension MapCell : MKMapViewDelegate{
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let eventAnnotation = view.annotation {
//            let selectedAnnotation = eventAnnotation.annotation
            let annotationsToRemove = mapView.annotations.filter { $0 !== eventAnnotation }
            mapView.removeAnnotations( annotationsToRemove )
        }
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        let annotationsToRemove = mapView.annotations
        mapView.removeAnnotations( annotationsToRemove )
        setupAnnotations()
    }
    
    
    
    
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView,
                 calloutAccessoryControlTapped control: UIControl) {
        let location = view.annotation as! CarMarker
        let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
        location.mapItem().openInMaps(launchOptions: launchOptions)
    }
    
    
//    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
//        // 2
//        guard let annotation = annotation as? CarMarker else { return nil }
//        // 3
//        let identifier = "marker"
//        var view: MKMarkerAnnotationView
//        // 4
//        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
//            as? MKMarkerAnnotationView {
//            dequeuedView.annotation = annotation
//            view = dequeuedView
//        } else {
//            // 5
//            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
//            view.canShowCallout = true
//            view.calloutOffset = CGPoint(x: -5, y: 5)
//            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
//        }
//        return view
//    }
}


