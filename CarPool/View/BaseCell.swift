//
//  BaseCell.swift
//  CarPool
//
//  Created by Apple on 9/14/18.
//  Copyright © 2018 iSwiftful. All rights reserved.
//

import UIKit

class BaseCell: UICollectionViewCell{
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    func setupViews(){
        
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
