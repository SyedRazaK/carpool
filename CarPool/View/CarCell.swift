//
//  CarCell.swift
//  CarPool
//
//  Created by Apple on 9/14/18.
//  Copyright © 2018 iSwiftful. All rights reserved.
//

import UIKit
import FontAwesome_swift

class CarCell: BaseCell {
    var index: Int?
    var placeMark: PlaceMark?{
        didSet{
            carTitle.text = placeMark?.name
            lblDescription.text = placeMark?.address
            lblFuelValue.text = "\(String(describing: placeMark!.fuel))%"
            lblCarType.text = placeMark?.engineType
            if (placeMark?.interior == "GOOD"){
                lblInteriorValue.text = "\(String.fontAwesomeIcon(name: .thumbsUp))" //  //
            }
            else{
                lblInteriorValue.text = "\(String.fontAwesomeIcon(name: .thumbsDown))" //  //
            }
            if(placeMark?.exterior == "GOOD"){
                lblExteriorValue.text = "\(String.fontAwesomeIcon(name: .thumbsUp))" //  //
            }
            else{
                lblExteriorValue.text = "\(String.fontAwesomeIcon(name: .thumbsDown))" //  //
            }
        }
    }
    let containerView : UIView = {
        let vw = UIView()
        vw.backgroundColor = UIColor(r: 250, g: 250, b: 250)
        vw.installShadow()
        vw.translatesAutoresizingMaskIntoConstraints = false
        return vw
    }()
    
    let midSeparatorView: UIView = {
        let vw = UIView()
        vw.backgroundColor = UIColor(r: 211, g: 211, b: 211)
//        vw.layer.cornerRadius = 1
        vw.translatesAutoresizingMaskIntoConstraints = false
        return vw
    }()
    
    let lblCarType : UILabel = {
        let lbl = UILabel()
        lbl.text = "CE"
        lbl.textColor = .gray
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        //        lbl.backgroundColor = .blue
        lbl.textAlignment = .left
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let thumbnailImageView : UIImageView = {
        let imgVw = UIImageView()
        imgVw.image = UIImage(named: "ic_car")
        imgVw.contentMode = .scaleAspectFit
//        imgVw.backgroundColor = .red
//        imgVw.installShadow()
        imgVw.translatesAutoresizingMaskIntoConstraints = false
        return imgVw
    }()
    
    let carTitle : UILabel = {
        let lbl = UILabel()
        lbl.text = "HH-GO8522"
        lbl.textColor = .black
        lbl.font = UIFont.boldSystemFont(ofSize: 14)
        //        lbl.backgroundColor = .blue
        lbl.textAlignment = .left
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let lblDescription : UILabel = {
        let lbl = UILabel()
        lbl.text = "Lesserstraße 170, 22049 Hamburg" //  //
        lbl.textColor = UIColor(r: 102, g: 102, b: 102)//.black //UIColor(r: 102, g: 102, b: 102)
        lbl.font = UIFont.systemFont(ofSize: 12)
        lbl.textAlignment = .left
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let lblInteriorTitle : UILabel = {
        let lbl = UILabel()
        lbl.text = "Interior" //  //
        lbl.textColor = UIColor(r: 102, g: 102, b: 102)//.black //UIColor(r: 102, g: 102, b: 102)
        lbl.font = UIFont.systemFont(ofSize: 10)
        lbl.textAlignment = .left
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let lblExteriorTitle : UILabel = {
        let lbl = UILabel()
        lbl.text = "Exterior" //  //
        lbl.textColor = UIColor(r: 102, g: 102, b: 102)//.black //UIColor(r: 102, g: 102, b: 102)
        lbl.font = UIFont.systemFont(ofSize: 10)
        lbl.textAlignment = .left
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    let lblFuelTitle : UILabel = {
        let lbl = UILabel()
        lbl.text = "Fuel" //  //
        lbl.textColor = UIColor(r: 102, g: 102, b: 102)//.black //UIColor(r: 102, g: 102, b: 102)
        lbl.font = UIFont.systemFont(ofSize: 10)
        lbl.textAlignment = .left
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let lblFuelValue : UILabel = {
        let lbl = UILabel()
        lbl.text = "42%" //  //
        lbl.textColor = UIColor(r: 102, g: 102, b: 102)
        lbl.font = UIFont.boldSystemFont(ofSize: 20)
        lbl.textAlignment = .left
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let lblInteriorValue : UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.fontAwesome(ofSize: 24)
        lbl.text = "\(String.fontAwesomeIcon(name: .thumbsDown))" //  //
        lbl.textColor = UIColor(r: 102, g: 102, b: 102)//.black //UIColor(r: 102, g: 102, b: 102)
        //        lbl.font = UIFont.systemFont(ofSize: 14)
        lbl.textAlignment = .left
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let lblExteriorValue : UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.fontAwesome(ofSize: 24)
        lbl.text = "\(String.fontAwesomeIcon(name: .thumbsUp))" //  //
        lbl.textColor = UIColor(r: 102, g: 102, b: 102)//.black //UIColor(r: 102, g: 102, b: 102)
        //        lbl.font = UIFont.systemFont(ofSize: 14)
        lbl.textAlignment = .left
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    
    func setupContainerView(){
        // need x,y, height, Width
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-4-[v0]-4-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": containerView]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-4-[v0]-2-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": containerView]))
        
        containerView.addSubview(carTitle)
        containerView.addSubview(lblDescription)
        containerView.addSubview(midSeparatorView)
        
        containerView.addSubview(lblExteriorValue)
        containerView.addSubview(lblExteriorTitle)
        containerView.addSubview(lblInteriorValue)
        containerView.addSubview(lblInteriorTitle)
        
        containerView.addSubview(lblFuelValue)
        containerView.addSubview(lblFuelTitle)
        containerView.addSubview(lblCarType)
        
        setupCarTitleView()
        setupDescription()
        setupMidSeparator()
        setupExteriorValueAndTitle()
        setupInteriorValueAndTitle()
        setupFuelValueAndTitle()
        setupCarType()
    }
    
    func setupCarTitleView() {
        carTitle.leftAnchor.constraint(equalTo: thumbnailImageView.rightAnchor, constant: 20).isActive = true
        carTitle.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 15).isActive = true
        
        carTitle.widthAnchor.constraint(equalTo: containerView.widthAnchor, multiplier: 2/3).isActive = true
        carTitle.heightAnchor.constraint(equalToConstant: 14).isActive = true
    }
    
    func setupDescription() {
        lblDescription.leftAnchor.constraint(equalTo: thumbnailImageView.rightAnchor, constant: 20).isActive = true
        lblDescription.topAnchor.constraint(equalTo: carTitle.bottomAnchor, constant: 5).isActive = true
        
        lblDescription.widthAnchor.constraint(equalTo: containerView.widthAnchor, multiplier: 2/3).isActive = true
        lblDescription.heightAnchor.constraint(equalToConstant: 14).isActive = true
    }
    
    func setupMidSeparator(){
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[v0]-10-[v1]-4-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": thumbnailImageView, "v1": midSeparatorView]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[v0]-5-[v1(1)]", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": lblDescription,"v1": midSeparatorView]))
    }
    
    func setupExteriorValueAndTitle(){
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[v0]-50-[v1]", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": thumbnailImageView, "v1": lblExteriorValue]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[v0]-8-[v1]", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": midSeparatorView,"v1": lblExteriorValue]))
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[v0]-45-[v1]", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": thumbnailImageView, "v1": lblExteriorTitle]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[v0]-10-[v1]", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": lblExteriorValue,"v1": lblExteriorTitle]))
        
    }
    
    func setupInteriorValueAndTitle(){
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[v0]-50-[v1]", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": lblExteriorValue, "v1": lblInteriorValue]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[v0]-8-[v1]", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": midSeparatorView,"v1": lblInteriorValue]))
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[v0]-35-[v1]", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": lblExteriorTitle, "v1": lblInteriorTitle]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[v0]-10-[v1]", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": lblInteriorValue,"v1": lblInteriorTitle]))
    }
    
    func setupFuelValueAndTitle() {
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[v0]-40-[v1]", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": lblInteriorValue, "v1": lblFuelValue]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[v0]-8-[v1]", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": midSeparatorView,"v1": lblFuelValue]))
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[v0]-37-[v1]", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": lblInteriorTitle, "v1": lblFuelTitle]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[v0]-10-[v1]", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": lblFuelValue,"v1": lblFuelTitle]))
    }
    
    func setupCarType() {
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-35-[v0]", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": lblCarType]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[v0]-0-[v1]", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": thumbnailImageView,"v1": lblCarType]))
    }
    
    func setupThumbnailView(){
        thumbnailImageView.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 10).isActive = true
        thumbnailImageView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 30).isActive = true
        
        thumbnailImageView.widthAnchor.constraint(equalTo: containerView.widthAnchor, multiplier: 1/5).isActive = true
        thumbnailImageView.heightAnchor.constraint(equalTo: containerView.heightAnchor, multiplier: 1/2).isActive = true
    }
    
    override func setupViews() {
        addSubview(containerView)
        addSubview(thumbnailImageView)
        
        setupContainerView()
        setupThumbnailView()
        
    }
    
}
