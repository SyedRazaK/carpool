//
//  FeedCell.swift
//  CarPool
//
//  Created by Apple on 9/16/18.
//  Copyright © 2018 iSwiftful. All rights reserved.
//

import UIKit

class FeedCell: BaseCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .white
        cv.delegate = self
        cv.dataSource = self
        return cv
    }()
    
    let cellId = "cellId"
    
    override func setupViews() {
        super.setupViews()
        if (CPStore.sharedInstance.placeMarks.count < 1){
            self.fetchPlaceMarks()
        }
        
        addSubview(collectionView)
        addConstraintsWithFormat(format: "H:|[v0]|", views: collectionView)
        addConstraintsWithFormat(format: "V:|[v0]|", views: collectionView)
        collectionView.register(CarCell.self, forCellWithReuseIdentifier: cellId)
    }
    
    func fetchPlaceMarks(){
        CPProgressView.shared.showProgressView()
        CarPoolAPI.sharedInstance.loadCarLocations({(data, error) in
            if (error == nil) {
                if data != nil{
                    CPProgressView.shared.hideProgressView()
                    CPStore.sharedInstance.updateStore()
                    self.collectionView.reloadData()
                }
            }
            else{
                print(error)
                return
                
            }
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return CPStore.sharedInstance.placeMarks.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! CarCell
        cell.placeMark = CPStore.sharedInstance.placeMarks[indexPath.item]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width, height: 120)
    }
    
}
