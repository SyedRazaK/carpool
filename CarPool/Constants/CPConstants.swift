//
//  CPConstants.swift
//  CarPool
//
//  Created by Apple on 9/15/18.
//  Copyright © 2018 iSwiftful. All rights reserved.
//

import Foundation
import UIKit

struct CPConstants {
    static var baseURL = "​https://s3-us-west-2.amazonaws.com/wunderbucket"
    struct AlertViewTypes {
        struct Info {
            static let ErrorTitle = "Error"
            static let invalidEntry = "Invalid Entry"
            static let serverError = "Server Error"
             static let serverConnectionFail =  "Unable to connect to server. Please check your internet connection"
            static let NoInternet   = "No Internet Connection"
            static let NotConnectToServer = "Could not connect to the server."
            static let dimissButtonTitle = "Ok"
            static let emptyURL = "Please set URL from SMACC app settings"
        }
    }
    
    
}

