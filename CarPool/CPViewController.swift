//
//  ViewController.swift
//  CarPool
//
//  Created by Apple on 9/14/18.
//  Copyright © 2018 iSwiftful. All rights reserved.
//

import UIKit

class CPViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    override var preferredStatusBarStyle : UIStatusBarStyle { return UIStatusBarStyle.lightContent }
    
    lazy var menuBar : MenuBar = {
        let mb = MenuBar()
        mb.homeController = self
        return mb
    }()
    
    let cellId = "cellId"
    let mapId = "mapId"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        self.setNeedsStatusBarAppearanceUpdate()
        // Set searchController properties
        definesPresentationContext = true
        setupNavigationTitle()
        setupCollectionView()
        self.setupMenuBar()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        self.setContentInset()
        self.collectionView?.reloadData()
    }

    func setContentInset(){
        collectionView?.contentInset = UIEdgeInsetsMake(50, 0, 0, 0)
        collectionView?.scrollIndicatorInsets = UIEdgeInsetsMake(50, 0, 0, 0)
    }
    func setupMenuBar(){
        view.addSubview(menuBar)
        view.addConstraintsWithFormat(format: "H:|[v0]|", views: menuBar)
        view.addConstraintsWithFormat(format: "V:|[v0(50)]|", views: menuBar)
    }
    
    func setupNavigationTitle(){
        navigationItem.title = "CAR PooL"
        navigationController?.navigationBar.isTranslucent = false
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 30, height: 45))
        titleLabel.text = "CAR PooL"
        titleLabel.textColor = .white
        titleLabel.font = UIFont(name: "Lato", size: 18)
        navigationItem.titleView = titleLabel
    }
    
    func setupNavbarButtons(){
        //        let navMenu = UIImage(named: "ic_menu")?.withRenderingMode(.alwaysOriginal)
        //        let menuBarButtonItem = UIBarButtonItem(image: navMenu, style: .plain, target: self, action: #selector(handleMenu))
        //        navigationItem.leftBarButtonItem = menuBarButtonItem
//        let navMore = UIImage.fontAwesomeIcon(name: .cog, textColor: .white, size: CGSize(width: 30, height: 30)).withRenderingMode(.alwaysOriginal)
//        let navBarButtonItem = UIBarButtonItem(image: navMore, style: .plain, target: self, action: #selector(handleMore))
//        let searchImage = UIImage(named: "search_icon")?.withRenderingMode(.alwaysOriginal)
//        let searcbBarButtonItem = UIBarButtonItem(image: searchImage, style: .plain, target: self, action: #selector(handleSearch))
        
//        navigationItem.rightBarButtonItems  = [navBarButtonItem,searcbBarButtonItem]
    }
    
    
    
    func setupCollectionView(){
        if let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout{
            flowLayout.scrollDirection = .horizontal
            flowLayout.minimumLineSpacing = 0
        }
        collectionView?.backgroundColor = .white
        collectionView?.register(FeedCell.self, forCellWithReuseIdentifier: cellId)
        collectionView?.register(MapCell.self, forCellWithReuseIdentifier: mapId)
//        collectionView?.register(CarCell.self, forCellWithReuseIdentifier: "CellId")
//        collectionView?.register(FeedCell.self, forCellWithReuseIdentifier: cellId)
//        collectionView?.register(FeedFavCell.self, forCellWithReuseIdentifier: groupFavId)
//
        setContentInset()
        collectionView?.isPagingEnabled = true
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2//CPStore.sharedInstance.placeMarks.count
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.item == 1 {
            return collectionView.dequeueReusableCell(withReuseIdentifier: mapId, for: indexPath) //as! CarCell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) //as! CarCell
        cell.backgroundColor = .white
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height - 50)
    }
    
    // Collection View Events
    func scrollToMenuIndex(menuIndex: Int){
        let indexPath = IndexPath(item: menuIndex, section: 0)
        collectionView?.scrollToItem(at: indexPath, at: [], animated: true)
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //print(scrollView.contentOffset.x)
        let xValue = scrollView.contentOffset.x
        menuBar.horizontalBarLeftAnchorConstraint?.constant = xValue/2
    }
    
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let index = targetContentOffset.pointee.x / view.frame.width
        let indexPath = IndexPath(item: Int(index), section: 0)
        
        menuBar.collectionView.selectItem(at: indexPath, animated: true, scrollPosition: [])
    }
    
    
    class func displayAlertInfoBox(_ title:String, message:String){
        //        NSCustomLoader.hide()
        let refreshAlert = UIAlertView()
        refreshAlert.title = title
        refreshAlert.message = message
        refreshAlert.addButton(withTitle: CPConstants.AlertViewTypes.Info.dimissButtonTitle)
        refreshAlert.show()
        //        let userSettings = POSAPI.sharedInstance.getUserSettings(POSAPI.sharedInstance.getCounterSettings().SalesmanCode)
        //        if (userSettings?.AlertSound)!{
        //            AudioServicesPlaySystemSound(SmaccConstants.alertTunes.errorTone)
        //        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

