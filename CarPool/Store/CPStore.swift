//
//  CPStore.swift
//  CarPool
//
//  Created by Apple on 9/16/18.
//  Copyright © 2018 iSwiftful. All rights reserved.
//

import Foundation

class CPStore : NSObject{
    static let sharedInstance = CPStore()
//    var filteredGroups = [DocumentGroup]()
    var placeMarks         = [PlaceMark]()
    override init() {
        super.init()
        placeMarks = CarPoolAPI.sharedInstance.getPlaceMarks() as! [PlaceMark]
    }
    
    func updateStore(){
        placeMarks = CarPoolAPI.sharedInstance.getPlaceMarks() as! [PlaceMark]
    }
}
