//
//  DataModel.swift
//  CarPool
//
//  Created by Apple on 9/15/18.
//  Copyright © 2018 iSwiftful. All rights reserved.
//

import UIKit
import RealmSwift

class PlaceMark: Object{
    @objc dynamic var address = ""
    @objc dynamic var engineType = ""
    @objc dynamic var exterior = ""
    @objc dynamic var fuel = 0
    @objc dynamic var interior = ""
    @objc dynamic var name = ""
    @objc dynamic var vin = ""
    @objc dynamic var cords: Coordinates?  = Coordinates()
    
    override static func primaryKey() -> String? {
        return "name"
    }
}

class Coordinates : Object {
    @objc dynamic var lon = 0.0
    @objc dynamic var lat = 0.0
    @objc dynamic var speed = 0.0
}

