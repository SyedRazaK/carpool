//
//  UIView+ConstraintWithFormat.swift
//  CarPool
//
//  Created by Apple on 9/14/18.
//  Copyright © 2018 iSwiftful. All rights reserved.
//

import UIKit

extension UIView {
    func addConstraintsWithFormat(format: String, views: UIView...) {
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))
    }
    
    func autoRemoveConstraint(_ constraint : NSLayoutConstraint?) {
        if constraint != nil {
            self.removeConstraint(constraint!)
        }
    }
    
}

