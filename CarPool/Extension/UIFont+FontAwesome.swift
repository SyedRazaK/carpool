//
//  UIFont+FontAwesome.swift
//  CarPool
//
//  Created by Apple on 9/15/18.
//  Copyright © 2018 iSwiftful. All rights reserved.
//
import UIKit
import CoreText
import FontAwesome_swift

public extension UIFont {
    
    /// Get a UIFont object of FontAwesome.
    ///
    /// - parameter ofSize: The preferred font size.
    /// - returns: A UIFont object of FontAwesome.
    public class func fontAwesome(ofSize fontSize: CGFloat) -> UIFont {
        let name = "FontAwesome"
        if UIFont.fontNames(forFamilyName: name).isEmpty {
            FontLoader.loadFont(name)
        }
        
        return UIFont(name: name, size: fontSize)!
    }
}
